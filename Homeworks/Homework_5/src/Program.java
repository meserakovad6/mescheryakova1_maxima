
import java.util.Random;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        int arrayCount = scanner.nextInt();
        int[] array = new int[arrayCount];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.round(Math.random() * 40);
            System.out.print(array[i] + ",");
        }

    }
}

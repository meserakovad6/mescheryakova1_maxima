import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        int arrayCount = scanner.nextInt();
        int[] array = new int[arrayCount];
        int index = 4;
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.round(Math.random() * 40);
            System.out.print(array[i] + ",");
        }
        System.out.print("Новый массив");
        array = Arrays.copyOf(array, array.length + 1);
        for (int i = array.length - 1; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = 10;
        for (int i = 0; i < array.length; i++) {
            System.out.print( array[i] + ",");
        }

    }
}

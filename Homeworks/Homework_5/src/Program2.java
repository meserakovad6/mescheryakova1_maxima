public class Program2 {
    public static void main(String[] args) {
        int [][] twoDimArray = {{3,6,4,27}, {4,7,23,12}, {4,0,19,4}};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            summDiag(twoDimArray);
            System.out.println();
        }
    }
    public static void summDiag(int twoDimArray[][]){
        int summDiag=0;
        summDiag += twoDimArray[0][0]+twoDimArray[1][1]+twoDimArray[2][2];
        System.out.println("Сумма елементов главной диагонали равна: " + summDiag );
    }
}

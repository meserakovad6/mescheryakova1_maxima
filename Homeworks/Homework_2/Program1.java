import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
    	
		int count = 0;
		int a = 10;

		while (a > 0) {
			int currentNumber = scanner.nextInt();

			int digitsSum = 0;

			while (currentNumber != 0 ) {
				digitsSum += currentNumber % 10;
				currentNumber = currentNumber / 10;
			}

			boolean isPrime = true;
			if (digitsSum < 18) {
				isPrime = false;
			}

			if (isPrime == false) {
				count++;
			}
			a--;
	    }

	    System.out.println(count);
	}
}

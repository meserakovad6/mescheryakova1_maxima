

public class Channal {
    private final int MAX_TVPROGRAM_COUNT = 1;
    private String channelName;
    private int number;
    private boolean onTV;
    private TV tv;
    private int tvprogramCount = 0;
    private TVprogram[] tVprograms;

    public Channal(int number, String channelName) {
        this.channelName = channelName;
        this.number = number;
        this.onTV = false;
        this.tVprograms = new TVprogram[MAX_TVPROGRAM_COUNT];
    }
    public void goToTV(TV tv){
        if(!onTV){
            if(tv.getChannal(this)){
                onTV = true;
                this.tv =tv;
            }
        } else {
            System.err.println("Канал" + getNumber() + getChannelName() + "Уже включен");
            onTV = false;
        }
    }
    public boolean getTVprogram(TVprogram tVprogram){
        if (tvprogramCount < 1){
            tVprograms[tvprogramCount] = tVprogram;
            System.out.println("Вы включили" + tVprogram.getTitle() + "На канале" + getChannelName());
            tvprogramCount++;
            return true;
        } else {
            tvprogramCount++;
            return false;
        }
    }

    public int getNumber() {
        return number;
    }

    public String getChannelName() {
        return channelName;
    }

    public boolean isOnTV() {
        return onTV;
    }
}

public class TV {
    private final int MAX_CHANNAL_COUNT = 1;
    private String name;
    public Channal[] channals;
    private int channalCount;
    private RemoteController remoteController;

    public TV(String name) {
        this.name = name;
        this.channals = new Channal[MAX_CHANNAL_COUNT];
        this.channalCount = 0;
    }
    public boolean getChannal(Channal channal){
        if (channalCount < MAX_CHANNAL_COUNT){
            channals[channalCount] = channal;
            System.out.println("Включен канал" + channal.getChannelName());
            channalCount++;
            return true;
        } else {
            System.out.println("Вы переключили на другой канал" + channal.getNumber() + channal.getChannelName());
            return false;
        }
    }

    public String getName() {
        return name;
    }
}

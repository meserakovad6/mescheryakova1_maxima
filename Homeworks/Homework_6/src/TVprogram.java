public class TVprogram {
    private String title;
    private boolean onChennal;
    private Channal chennal;

    public TVprogram(String title) {
        this.title = title;
        this.onChennal = false;
    }
    public void goToChannal(Channal channal){
        if (!onChennal){
            if(channal.getTVprogram(this)){
                onChennal = true;
                this.chennal = channal;
            }
        } else {
            onChennal = false;
        }
    }

    public String getTitle() {
        return title;
    }

    public Channal getChennal() {
        return chennal;
    }

    public boolean isOnChennal() {
        return onChennal;
    }
}

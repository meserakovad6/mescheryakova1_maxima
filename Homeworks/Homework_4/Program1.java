import java.util.Scanner;
  
  class Program1 {
  	   
    public static int parse (int array[]) {
    	Scanner scanner = new Scanner(System.in);
		int a = 1;
		int parse= 0;
		for (int i = 0; i < array.length; i++) {
			parse += (array[i] % 10) * a;
			a = a * 10;
		}

    	return parse;
    }
    public static void main(String[] args) {
    	int[] array = { 4, 5, 4, 6};
		int number = parse(array);
		System.out.println(number);
    } 
}  
